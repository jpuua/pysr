#!/usr/bin/python
import sys
from subprocess import Popen, PIPE
import thread
import time

class SpeechReco:
    direction = ""
    def __init__(self):
        print "init"   
    
    def start(self):
        print "start"
        command ='pocketsphinx_continuous -hmm /et/models/hmm/est16k.cd_ptm_1000-mapadapt -jsgf /et/models/lm/pyrobot.jsgf -dict /et/robot.dict'
        #, universal_newlines = True
        pocketsphinx = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)  
        thread.start_new_thread(self.listen_to_pocketsphinx, (pocketsphinx,))
        
    def listen_to_pocketsphinx(self,pocketsphinx):
        while True:
            line = pocketsphinx.stdout.readline()
            if line.startswith("000"):
              sentence = line.partition(": ")[2].strip()
              if sentence:
                  self.direction = sentence
              else:
                  self.direction = ""

#To test without pyrobot uncomment following lines and run in terminal
# python speech.reco.py
#s = SpeechReco()
#s.start()
#while True:
    #if s.direction:
        #print s.direction
        #s.direction = ""

