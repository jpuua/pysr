#!/usr/bin/python
from pyrobot.brain import Brain  
import voice

import sys
from subprocess import Popen, PIPE
import thread
import time

class Avoid(Brain): 
   translation = 0
   rotate = 0
   #envir states 1 = free; 0 = blocked
   f = 0
   b = 0
   f_r = 0
   f_l = 0
   b_r = 0
   b_l = 0
   
   def resetEnvirStates(self):
      self.f = 0
      self.b = 0
      self.f_r = 0
      self.f_l = 0
      self.b_r = 0
      self.b_l = 0
      
   # gets called before first run
   def setup(self, **args):
      self.s = SpeechReco()
      #Starts speech listener
      self.s.start()         
      
   def getDirection(self, text):
      print text
      voice.sayText(text)
      invalid = True
      var = ""
      self.s.direction = ""
      while invalid:
         if self.s.direction:
            var = self.s.direction
            self.s.direction = ""
            #TODO: confirm input
            invalid = False
            print var         

      if var == "paremale":
         return (0.0, -0.5)
      elif var == "vasakule":
         return (0.0, 0.5)
      elif var == "tagasi":
         return (-0.5, 0.0)
      elif var == "otse":
         return (0.5, 0.0)     
   
   def getLenght(self, text):
      print text
      voice.sayText(text)
      invalid = True
      var = ""
      self.s.direction = ""
      while invalid:
         #Waits for valid input
         if self.s.direction:
            var = self.s.direction
            self.s.direction = ""
            #TODO: confirm input
            invalid = False
            print var
      if var == "uks":
         return 100
      elif var == "kaks":
         return 200
      elif var == "kolm":
         return 300
      elif var == "neli":
         return 400
      elif var == "viis":
         return 500
      else:
         return 0
      
   #Observe the world around and retunr current state 
   def determineMove(self, tolerance):
      #collect sensor data before commands
      front = min([s.distance() for s in self.robot.range["front"]])
      front_left = min([s.distance() for s in self.robot.range["left-front"]])
      front_right = min([s.distance() for s in self.robot.range["right-front"]])
      back = min([s.distance() for s in self.robot.range["back"]])
      back_left = min([s.distance() for s in self.robot.range["back"]])
      back_right = min([s.distance() for s in self.robot.range["back"]])
      
      if front < tolerance:
         self.f = 1
         return Texts.front
      elif front_left < tolerance:
         self.f = 1
         return Texts.front_left
      elif front_right < tolerance: 
         self.f = 1
         return Texts.front_right
      elif back < tolerance:
         self.b = 1
         return Texts.back
      elif back_left < tolerance:
         self.b = 1
         return Texts.back_left
      elif back_right < tolerance:
         self.b = 1
         return Texts.back_right      
      else:  
         return("0")

   def step(self):
      self.resetEnvirStates()
      #Stop previous move
      self.robot.move(0,0)
      #Look for obstacles
      sensors = self.determineMove(0.5)
      if sensors == "0": #all clear
         voice.sayText("All clear")
      else:
         voice.sayText(sensors)
      
      #Get voice commands...
      try:
         self.translation, self.rotate = self.getDirection("Tell me the direction")
         duration = self.getLenght("How far?")
      except:
         print "Failure"
         duration = 0
      #do what was told
      while duration > 0:
         move = self.makeMove(self.translation, self.rotate)
         if move == 0:
            duration = duration - 1
         else:
            duration = 0
            break
      self.robot.move(0, 0)
         

   def makeMove(self, t, r):
      print "makemove"
      if t > 0:
         #going forward
         front = min([s.distance() for s in self.robot.range["front"]])
         #front_left = min([s.distance() for s in self.robot.range["left-front"]])
         #front_right = min([s.distance() for s in self.robot.range["right-front"]])
         print front
         if front > 0.5:
            print "liigun edasi"
            self.robot.move(t, r)
         else:
            print "obstacle"
            self.robot.move(0, 0)
            return Texts.front
         
      if t < 0:
         back = min([s.distance() for s in self.robot.range["back"]])
         #back_left = min([s.distance() for s in self.robot.range["back"]])
         #back_right = min([s.distance() for s in self.robot.range["back"]])
         print back
         if back > 0.5:
            self.robot.move(t, r)
         else:
            print "obstacle"
            self.robot.move(0, 0)
            return Texts.back
         
      if t == 0:
         print "rotating"
         self.robot.move(t, r)
      return 0

def INIT(engine):  
   assert (engine.robot.requires("range-sensor") and
           engine.robot.requires("continuous-movement"))
   
   return Avoid('Avoid', engine)  



class Texts:

   front = "Obstacle ahead, you should reverse!"
   front_left = "Obstacle in front on the left, you should hold right!"
   front_right = "Obstacle in front on the right, you should hold left!"
   back = "Obstacle behind you should go forward or turn!"
   back_left = "Obstacle behind on the left, you should hold right!"
   back_right = "Obstacle behind on the right, you should hold left!"
   
      
class SpeechReco:
   direction = ""
   def __init__(self):
       print "init"   
   
   def start(self):
      print "start"
      command ='pocketsphinx_continuous -hmm ~/speech/et/models/hmm/est16k.cd_ptm_1000-mapadapt -jsgf ~/speech/et/models/lm/robot2.jsgf -dict ~/speech/et/robot.dict'
      thread.start_new_thread(self.listen_to_pocketsphinx, (Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE) ,))
       
   def listen_to_pocketsphinx(self,pocketsphinx):
      while True:
         line = pocketsphinx.stdout.readline()
         if line.startswith("000"):
            sentence = line.partition(": ")[2].strip()
            if sentence:
               self.direction = sentence
            else:
               self.direction = ""