# Pyrobot brain - Estonian speech recognition

Pocketsphinx scripts and code in "et" folder is written by Tanel Alumäe [(original source)][1]

_Except dictionary file and grammar file._

##Requirements

Linux OS
 
Following software is required before executing the program.
 
- Pyrobot [(download)][2]
- Festival [(download)][3]
- Pocketsphinx [(download)][4]
- Sphinxbase [(download)][5]


##Using speech reconizing Pyrobot brain 

Download the source and extract it in:

    pyrobot/plugins/brains/

After exracting start Pyrobot from terminal with command

    pyrobot
	
or (path to pyrobot directory)

    pyrobot/bin/pyrobot

When Pyrobot is started select following stuff from UI:

- Server - PyrobotSimulator
- World (environment) - Tutorial.py (also other worlds are supported)
- Robot - PyrobotRobot.py
- Brain - (Directories) pysr -> listening_brain.py


If everything was correct then following lines should appear in the console of the Pyrobot:

    Loaded 'path_to_directory/pysr/listening_brain.py'
	init
	start

Now you can press Run and listen to the commands from robot and act accordingly
 

[1]:https://github.com/alumae/et-pocketsphinx-tutorial
[2]:http://pyrorobotics.com/?page=PyroSoftware
[3]:http://www.cstr.ed.ac.uk/projects/festival/
[4]:http://cmusphinx.sourceforge.net/wiki/download/
[5]:http://cmusphinx.sourceforge.net/wiki/download/